import java.util.*;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Description:
 * Author     :
 * License    :
 *
 *
 * For more information see: https://en.wikipedia.org/wiki/Dijkstra's_algorithm#Using_a_priority_queue
 *
 */


class Dijkstra implements SearchAlgorithm {
  // ---------------------------------------------------------------------------
  private HashMap<String, String> previous;         // Mappa l'etichetta precedente
  private HashMap<String, Integer> distances;       // Distanza sommata dei Vertici precedenti
  private PriorityQueue<Vertex> available;          // Vertici ancora disponibili
  private HashSet<Vertex> visited;                  // Vertici visitati e da non verificare nuovamente
  private String startVertex;                       // Etichetta del Vertice iniziale
  private Graph graph;                              // Grafo
  // ---------------------------------------------------------------------------
  
  
 /**
  * Costruttore, inizializza le mappe
  *
  * @param  graph        rappresenta la logica del grafo
  * @param  startVertex  vertice da cui la ricerca ha inizio
  * 
  */
   
  // ---------------------------------------------------------------------------
  Dijkstra(Graph graph, String startVertex) {
    this.graph       = graph;
    this.startVertex = startVertex;
    
    visited   = new HashSet<Vertex>();
    previous  = new HashMap<String, String>();
    distances = new HashMap<String, Integer>();
    
    // Inizializzo la coda con priorita' (ordinamento crescente)
    available = new PriorityQueue<Vertex>(graph.getNumberOfVertices(), new Comparator<Vertex>() {
      public int compare(Vertex one, Vertex two) {
        return distances.get(one.getLabel()) - distances.get(two.getLabel()); 
      }
    });
    
    // Inizializzo la distanza del primo vertice con sè stesso (0, per l'appunto)
    distances.put(startVertex, 0);
    
    /*
     * Inizializzo i valori a null ed a valore "infinito".
     * Inserisco anche nella coda di priorita' i vertici
     *
     */
    for(Vertex v : graph.getVertices()) {
      if(!v.getLabel().equals(startVertex)) {
        previous.put(v.getLabel(), null);
        distances.put(v.getLabel(), Integer.MAX_VALUE);
      }
      
      available.add(v);
    }
  }
  // -----------------------------------------------------------------------------
  
  
  
  // Dijakstra search algorithm
  @Override
  public void calculate() {
      
    while(!available.isEmpty()) {
      Vertex minVertex = available.poll();
        
      for(Edge e : minVertex.getNeighbors()) {
        Vertex v = e.getOpposite(minVertex.getLabel());
        
        if(visited.contains(v)) continue;
        
        int newDist = distances.get(minVertex.getLabel()) + e.getWeight();
          
        if(newDist < distances.get(v.getLabel())) {
          distances.remove(v.getLabel());
          distances.put(v.getLabel(), newDist);
          
          previous.remove(v.getLabel());
          previous.put(v.getLabel(), minVertex.getLabel());
          
          available.remove(v);
          available.add(v);
        }
      }
      
      visited.add(minVertex);
        
    }
  }
  
  public int getDistance(String label) {
    return distances.get(label);
    
  }
  
  public List<Vertex> getPath(String label) {
	  LinkedList<Vertex> path = new LinkedList<Vertex>();
	  path.add(graph.getVertexFromLabel(label));
    
	  while(!label.equals(startVertex)){
	    Vertex predecessor = graph.getVertexFromLabel(previous.get(label));
	    label              = predecessor.getLabel();
      
	    path.add(0, predecessor);
	  }

	  return path;
  }
  

}