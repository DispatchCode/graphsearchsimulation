import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


import java.util.*;

class DrawArea extends JPanel implements MouseListener{
  
  private LinkedList<VertexEllipse> vertexGuiLists;
  private LinkedList<EdgeLine> edgeGuiLists;
  private Random r;
  
  DrawArea() {
    super(new BorderLayout());
    vertexGuiLists = new LinkedList<VertexEllipse>();
    edgeGuiLists = new LinkedList<EdgeLine>();
    
    setOpaque(true);
    setBackground(Color.WHITE);
    
    addMouseListener(this);
    
    r = new Random();
    
  }
  
  void setVertices(Set<Vertex> vertices) {
    for(Vertex v : vertices) {
      VertexEllipse ve = new VertexEllipse(v.getLabel(), r.nextInt(750),r.nextInt(750));
      vertexGuiLists.add(ve);

    }
  }
  
  void setEdges(Set<Edge> edges) {
    for(Edge e : edges) {
      Vertex v1 = e.getTo(), v2 = e.getFrom();
      edgeGuiLists.add(new EdgeLine(getGraphicPosition(v1.getLabel()), getGraphicPosition(v2.getLabel()), e.getWeight()));
    }

  }
  
  void drawGraph() {
    repaint();
  }
  
  
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    
    Graphics2D g2 = (Graphics2D) g;
    g2.setColor(Color.RED);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    
    g2.setStroke(new BasicStroke(5));
    if(edgeGuiLists != null) {
      for(EdgeLine el : edgeGuiLists) {
        g2.setColor(Color.BLACK);
        
        int xx = el.getX1() > el.getX2() ? -14 : 14;
        int yy = el.getY1() > el.getY2() ? -13 : 13;
        
        g2.drawLine(el.getX1()+xx, el.getY1()+yy, el.getX2()-xx, el.getY2()-yy);
        
        g2.setColor(Color.BLUE);
        g.setFont(new Font("Helvetica", Font.PLAIN, 18)); 
        
        int weightx = (el.getX1()+el.getX2())/2;
        int weighty = (el.getY1()+el.getY2())/2;
        int p       = weighty > weightx ? -20 : 20;
        
        g2.drawString(""+el.getWeight(), weightx+p,weighty+p ); 

      }
    }
    
    g2.setFont(new Font("Monospace", Font.PLAIN, 18)); 
    if(vertexGuiLists != null) {
      for(VertexEllipse ve : vertexGuiLists) {
        g2.drawImage(ve.getImage(), ve.getX(),ve.getY(), null);
        g2.drawString(ve.getLabel(), ve.getX()+30, ve.getY()+30);
        
      }
    }
    
  }
  
  
  private Point getGraphicPosition(String s) {
    for(VertexEllipse ve : vertexGuiLists) {
      if(ve.getLabel().equals(s)) {
        return new Point(ve.getX()+30, ve.getY()+30);
      }
    }
    return null;
  }
  
  
  
  
  
  public void mouseClicked(MouseEvent me) {
    System.out.println(me.getX()+" "+me.getY()+"\n");
    for(VertexEllipse ve : vertexGuiLists) {
      if(ve.contains(me.getX(), me.getY())) {
        System.out.println(ve.getLabel()+" ("+ve.getX()+", "+ve.getY()+")");
      }
    }
  }
  
  
  public void mouseEntered(MouseEvent me) {}
  public void mouseExited(MouseEvent me) {}
  public void mouseReleased(MouseEvent me) {}
  public void mousePressed(MouseEvent me) {}
}