import java.util.ArrayList;


public class Edge {
  private Vertex to, from;
  private int weight;
  
  public Edge(Vertex to, Vertex from, int weight) {
    this.weight = weight;
    this.to     = to;
    this.from   = from;
  }
  
  public Edge(Vertex to, Vertex from) {
    this(to, from, 1);
  }
  
  public Edge() {
    this.weight = 1;
  }
  
  
  // 
  
  boolean addVertex(Vertex to, Vertex from, int weight) {
    if(!to.getLabel().equals(from.getLabel())) {
      this.weight = weight;
      this.to     = to;
      this.from   = from;
      
      return true;
    }
    
    return false;
  }
  
  boolean addVertex(Vertex to, Vertex from) {
    return addVertex(to, from, 1);
  }
  
  Vertex getOpposite(String v) {
    return (v.equals(to.getLabel())) ? from : to;
  }
  
  boolean isVertexOfEdge(Vertex v1, Vertex v2) {
    return (to.getLabel().equals(v2.getLabel()) && from.getLabel().equals(v1.getLabel()) || 
            to.getLabel().equals(v1.getLabel()) && from.getLabel().equals(v2.getLabel())
           );
  }
  
  
  
  Vertex getTo() {
    return to;
  }
  
  Vertex getFrom() {
    return from;
  }
  
  int getWeight() {
    return weight;
  }
  
  public String toString() {
    return to.getLabel()+" "+from.getLabel();
    
  }
}