import java.awt.geom.*;
import java.awt.*;


class EdgeLine {
  private int x1, x2, y1, y2, weight;
  
  EdgeLine(Point p1, Point p2, int weight) {
    this.x1 = (int)p1.getX();
    this.y1 = (int)p1.getY();
    this.x2 = (int)p2.getX();
    this.y2 = (int)p2.getY();
    this.weight = weight;
    
  }
  
  
  int getX1() {
    return x1;
  }
  
  int getX2() {
    return x2;
  }
  
  int getY1() {
    return y1;
  }
  
  int getY2() {
    return y2;
  }
  
  int getWeight() {
    return weight;
  }
  
  
  
  
}