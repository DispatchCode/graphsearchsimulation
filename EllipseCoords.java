class EllipseCoords {
  private double x, y;
  
  
  EllipseCoords(double x, double y) {
    this.x = x;
    this.y = y;
  }
  
  double getX() {
    return x;
  }
  double getY() {
    return y;
  }
  
  void setX(double x) {
    this.x = x;
  }
  void setY(double y) {
    this.y = y;
  }

  public String toString() {
    return "("+x+", "+y+")";
  }
}