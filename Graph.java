import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;


public class Graph {
  private HashMap<Integer, Edge> edgeList;
  private HashMap<String, Vertex> vertexList;
  
  
  public Graph() {
    edgeList   = new HashMap<Integer, Edge>();
    vertexList = new HashMap<String, Vertex>();
  }
  
  public Graph(ArrayList<Vertex> vertex) {
    edgeList   = new HashMap<Integer, Edge>();
    vertexList = new HashMap<String, Vertex>();
    
    for(Vertex v : vertex) {
      vertexList.put(v.getLabel(), v);
    }
  }
  
  
  
  boolean addEdge(Vertex v1, Vertex v2) {
    return addEdge(v1, v2, 1);
  }
  
  boolean addEdge(Vertex v1, Vertex v2, int weight) {
    if(v1.equals(v2)) return false;
    
    Edge edge = new Edge(v1, v2, weight);
    if(edgeList.containsKey(edge.hashCode())) return false;
    else if(v1.containsEdge(edge) || v2.containsEdge(edge)) return false;
    
    edgeList.put(edge.hashCode(), edge);
    v1.addEdge(edge);
    v2.addEdge(edge);
    
    return true;
    
  }
  
  boolean addVertex(Vertex v) {
    if(vertexList.containsKey(v.getLabel())) {
      return false;
    }
    
    vertexList.put(v.getLabel(), v);
    
    return true;
  }
  
  boolean containsEdge(Edge edge) {
    return edgeList.containsKey(edge.hashCode());
  }
  
  Vertex getVertexFromLabel(String label) {
    for(Vertex v : getVertices()) {
      if(v.getLabel().equals(label)) {
        return v;
      }
    }
    
    return null;
  }
  
  
  int getWeight(Vertex v1, Vertex v2) {
    for(Edge e : getEdges()) {
      if(e.isVertexOfEdge(v1, v2)) return e.getWeight();
    }
    
    return -1;
    
  }

  
  Set<Edge> getEdges(){
    return new HashSet<Edge>(edgeList.values());
  }
  
  Set<Vertex> getVertices() {
    return new HashSet<Vertex>(vertexList.values());
  }
  
  Set<String> getLabels() {
    return new HashSet<String>(vertexList.keySet());
  }
  
  int getNumberOfVertices() {
    return vertexList.size();
  }
  
}