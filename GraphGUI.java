import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

class GraphGUI extends JFrame {
  private Graph graph;
  private DrawArea drawArea;
  
  GraphGUI() {
    super("Graphics Graph Rapresentation");
    setSize(800,800);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }
  
  void drawGraph(Graph graph) {
    this.graph = graph;
    
    if(drawArea != null) {
      remove(drawArea);
      revalidate();
      repaint();
    } else {
      drawArea = new DrawArea();
      add(drawArea);
    }
    
    // Ottengo gli archi del grafo
    Set<Edge> edgeList = graph.getEdges();
    // Ottengo i vertici
    Set<Vertex> vertexList = graph.getVertices();
    
    drawArea.setVertices(vertexList);
    drawArea.setEdges(edgeList);
    drawArea.drawGraph();

  }  

  
}