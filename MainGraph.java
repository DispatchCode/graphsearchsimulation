import java.util.ArrayList;
import java.util.Set;
import java.util.Random;


class MainGraph {
  public static void main(String[] args) {
    Graph graph = new Graph();
    Random r = new Random();
    
    Vertex[] v = new Vertex[5];
    for(int i=0; i<v.length; i++) {
      v[i] = new Vertex(""+i);
      graph.addVertex(v[i]);
    }
    
    graph.addEdge(v[0], v[1], r.nextInt(50));
    graph.addEdge(v[1], v[2], r.nextInt(50));
    graph.addEdge(v[0], v[3], r.nextInt(50));
    graph.addEdge(v[2], v[3], r.nextInt(50));
    graph.addEdge(v[4], v[0], r.nextInt(50));
    graph.addEdge(v[3], v[4], r.nextInt(50));
    graph.addEdge(v[2], v[4], r.nextInt(50));
    graph.addEdge(v[1], v[3], r.nextInt(50));
   // graph.addEdge(v[0], v[3]);
   // graph.addEdge(v[0], v[4]);
   // graph.addEdge(v[4], v[2]);
    
    
    for(int i=0; i<v.length; i++) {
      System.out.print("Label: "+v[i].getLabel()+" : ");
      System.out.println(v[i]);
      
    }
    
    System.out.println("\n---------------------------");
    // Edge prints
    Set<Edge> edgeList = graph.getEdges();
    for(Edge e : edgeList) {
      System.out.println(e);
    }
    
    System.out.println();
    
    Set<Vertex> vertexList = graph.getVertices();
    for(Vertex ve : vertexList) {
      System.out.println(ve.getLabel());
    }
    
    System.out.println("\n--------- Djakstra ---------");
    String labelTo = "3";
    
        
    Dijkstra d = new Dijkstra(graph, v[0].getLabel());
    d.calculate();
    d.printTest();
    
    
  }
}