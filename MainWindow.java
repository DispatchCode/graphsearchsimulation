import javax.swing.*;
import java.util.Random;


/*
 * EP dell'applicazione 
 *
 *
 */

class MainWindow {
  public static void makeGUI() {
    Random r = new Random();
    
    Graph graph = new Graph();
    
    // Creo 6 vertici e li aggiungo al grafo
    Vertex[] v = new Vertex[6];
    for(int i=0; i<v.length; i++) {
      v[i] = new Vertex(""+i);
      graph.addVertex(v[i]);
    }
    
    // Aggiungo gli archi tra i vertici, assegnando
    // all'arco un peso random
    graph.addEdge(v[0], v[1], r.nextInt(50));
    graph.addEdge(v[1], v[2], r.nextInt(50));
    graph.addEdge(v[0], v[3], r.nextInt(50));
    graph.addEdge(v[2], v[3], r.nextInt(50));
    graph.addEdge(v[4], v[0], r.nextInt(50));
    graph.addEdge(v[3], v[4], r.nextInt(50));
    graph.addEdge(v[2], v[4], r.nextInt(50));
    graph.addEdge(v[1], v[3], r.nextInt(50));
    graph.addEdge(v[2], v[5], r.nextInt(50));

    // Disegno il grafo sulla GUI
    GraphGUI gg = new GraphGUI();
    gg.drawGraph(graph);
    
    // Creo un'istanza di un algoritmo di ricerca, 
    // in questo caso Dijkstra 
    SearchAlgorithm sa = new Dijkstra(graph, v[0].getLabel()); // v[0] è la posizione di partenza
    sa.calculate();
    
    // v[5] è la destinazione; calcola il cammino più breve tra v[0] e v[5]
    for(Vertex v1 : sa.getPath(v[5].getLabel())) {
      System.out.print(v1.getLabel() +" -> ");
    }
    
    System.out.println("\nDistance: "+sa.getDistance(v[5].getLabel()));
    
  }
  
  
  public static void main(String[] args) {
    try {
      SwingUtilities.invokeAndWait(new Runnable() {
        public void run() {
          makeGUI();
        }
      });
    } catch(Exception e) {}
  }
}