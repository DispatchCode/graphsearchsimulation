# README #

A simple example of a graph implementation and his drawing. The graph is weighted, and many control drawing are not used.
There is also a Djakstra algorithm implementation.

## Instructions ##
The main class for the GUI application is *MainWindow.java*. Just compile and execute it.

## Screenshot ##

![graph.png](https://bitbucket.org/repo/8k9AnB/images/3813950188-graph.png)