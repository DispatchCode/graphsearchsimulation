import java.util.List;

/* 
 * Interfaccia utilizzata dalle classi di algoritmi di ricerca
 *
 *
 *
 */

interface SearchAlgorithm {
  void calculate();
  List<Vertex> getPath(String label);
  int getDistance(String label);
}