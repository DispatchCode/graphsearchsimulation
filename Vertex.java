import java.util.ArrayList;
import java.util.Set;
import java.util.List;
import java.util.HashSet;


public class Vertex {
  private ArrayList<Edge> edgeList;
  private String label;
  
  
  public Vertex(String label) {
    this.label = label;
    
    edgeList = new ArrayList<Edge>();
  }
  
  public Vertex(int label) {
    this(label+"");
  }
  
  
  String getLabel() {
    return label;
  }
  
  
  boolean containsEdge(Edge edge) {
    return edgeList.contains(edge);
  }
  
  boolean addEdge(Edge edge) {
    if(!containsEdge(edge)) {
      edgeList.add(edge);
      return true;
    }
    
    return false;
  }
  
  ArrayList<Edge> getEdges() {
    return edgeList;
  } 
 
  
  
  Edge getEdgeAt(int index) {
    if(index < 0 || index > edgeList.size()) return null;
    
    return edgeList.get(index);
  }
  
  
  List<Edge> getNeighbors() {
    return edgeList;
  }
  
  
  public String toString() {
    StringBuilder sb = new StringBuilder();
    
    for(Edge e : edgeList) {
      sb.append(e.hashCode() +" ("+e.getWeight()+"), ");
    }
    
    return sb.toString();
  }
  
}