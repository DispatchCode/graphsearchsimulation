import java.awt.*;
import javax.imageio.*;
import java.io.File;
import java.io.IOException;

class VertexEllipse {
  private String label;
  private int x, y, h, w;
  private Image img;
  
  VertexEllipse(String label, int x, int y) {
    this.label = label;
    this.x = x;
    this.y = y;

    try {
      img = (Image) ImageIO.read(new File("btn.png"));
      h = img.getHeight(null);
      w = img.getWidth(null);
    } catch (IOException e) {}
  }
  
  String getLabel() {
    return label;
  }
  
  
  Image getImage() {
    return img;
  }
  
  void setX(int x) {
    this.x = x;
  }
  void setY(int y) {
    this.y = y;
  }


  public int getX() {
    return x;
  }
  public int getY() {
    return y;
  }
  

  boolean contains(int mx, int my) {
    return mx >= x && mx <= x+62 && my >= y && my <= y+62;
  }

  
  public String toString() {
    return label;
  }
}